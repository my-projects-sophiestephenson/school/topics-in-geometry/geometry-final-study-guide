\documentclass[12pt, twoside]{amsart}

\newcommand{\R}{{\mathbb R}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\C}{{\mathbb C}}
\newcommand{\D}{{\mathbb D}}
\newcommand{\bigH}{{\mathbb H}}
\newcommand{\Rbar}{{\overline{\R}}}
\newcommand{\xn}{{\vec x_n}}

\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8.5in}
\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\topmargin}{0in}

\everymath{\displaystyle}
\usepackage{commath}
\usepackage{graphicx}
\graphicspath{ {./images/} }


\begin{document}


\centerline{\large \sc Geometry Final Study Guide}
\bigskip
\bigskip

\noindent \textbf{\large The Upper Half Plane}

\section{Normalized Mobius Transformations}
\bigskip
Normalizing a Mobius transformation simply means manipulating it so that the determinant is 1. For some $T(z) = \frac{az + b}{cz + d}$, also denoted $M_T = \begin{pmatrix} a & b \\ c & d \end{pmatrix},$ the determinant is clearly $\delta = ad - bc$. Thus, we can create 
$$M_{T_N} = \begin{pmatrix} a/\sqrt{\delta} & b/\sqrt{\delta} \\ c/\sqrt{\delta} & d/\sqrt{\delta} \end{pmatrix},$$
a normalized version of $T$ with determinant 1. \\

We must normalize a transformation $T$ before we can tell if it preserves $\bigH$ or $\Rbar$.\\

\bigskip
\section{The Cross Ratio}
\bigskip

The cross ratio of four points $z_0, z_1, z_2, z_3$ is denoted
$$(z_0, z_1, z_2, z_3) = \frac{(z_0 - z_2)(z_1 - z_3)}{(z_0 - z_3)(z_1 - z_2)}.$$

We can use the cross ratio to find transformations that take triples of points to any other triple of points. Define the transformation 
$$S(z) = (z, z_1, z_2, z_3) =  \frac{(z - z_2)(z_1 - z_3)}{(z - z_3)(z_1 - z_2)}.$$
 We find that $S(z_1) = 1, S(z_2) = 0,$ and $S(z_3) = \infty$.\\
 
 \begin{quote}\textit{
 	Ex) We want to find a transformation that takes 0 to -1, 1 to 1, and $i$ to $2i$. First, we define 
	$$T(z) = \frac{(z - 1)(0 - i)}{(z - i)(0 - 1)} = \frac{-iz + i}{-z + i}.$$\\
	We see that $T(0) = 1, T(1) = 0,$ and $T(i) = \infty$. Next, we define
	$$S(z) = \frac{(z - 1)(-1 - 2i)}{(z - 2i)(-1 - 1)} = \frac{(1/2 + i)z - (1/2 + i)}{z - 2i}.$$\\
	We see this time that $S(-1) = 1, S(1) = 1$, and $S(2i) = \infty$. Now, we simply calculate the transformation
	$$S^{-1} \circ T = \begin{pmatrix} -2i & 1/2 + i \\ -z & 1/2 + i \end{pmatrix} \begin{pmatrix} -i & i \\ -1 & i \end{pmatrix}.$$
	This transformation will behave as desired by construction.}
 \end{quote}
 
 \bigskip
\section{Constructing Hyperbolic Lines}
\bigskip

If we have two points $P$ and $Q$, where $Re(P) \not = Re(Q)$, we know that there exists a unique hyperbolic line (a circle orthogonal to $\Rbar$) between $P$ and $Q$. Assume that $P = x_P + iy_P$ and $Q = x_Q + iy_Q$. To construct the hyperbolic line between $P$ and $Q$, we follow these steps:\\

\begin{enumerate}
	\item Find the perpendicular bisector of $P$ and $Q$. By definition, we know that this is the line $|z - P| = |z - Q|.$ \\
	\item Find where this perpendicular bisector intersects the real axis, i.e. if $z = x + iy$, find where $y = 0$. This is the center of the circle.\\
	\item Find the radius by taking the distance between the center of the circle and $P$ or $Q$.\\
\end{enumerate}

\begin{quote}\textit{
	Consider the points $i$ and $1 + 2i$. We will find the equation for the hyperbolic line between them. \\
	\begin{enumerate}
		\item The perpendicular bisector is $|z - i| = |z - (1 + 2i)|$. We can rewrite this as
		\begin{align*}
			|z - i| = |z - (1 + 2i)| &\implies x^2 + (y - 1)^2 = (x-1)^2 + (y-2)^2\\
			&\implies x^2 + y^2 -2y + 1 = x^2 - 2x + 1 + y^2 - 4y + 4\\
			&\implies 0 = -2x - 2y + 4\\
			&\implies 0 = x + y - 2
		\end{align*}
		\item Setting $y = 0$, we find that the line is $0 = x - 2$, so the center $c$ is $2$.\\
		\item The radius $r$ is $|2 - i| = \sqrt{4 + 1} = \sqrt{5}$. \\
	\end{enumerate}
	Thus, the equation for the hyperbolic line between $i$ and $1 + 2i$ is $|z - 2| = \sqrt{5}$. The endpoints at infinity are $2 + \sqrt{5}$ and $2 - \sqrt{5}$.
	}
\end{quote}

\bigskip
\section{Isometries of the Upper Half Plane}
\bigskip

Consider a transformation $T$ which is normalized ($ad - bc = 1$). We saw that $T$ preserves $\Rbar$ if all of its coefficients are real or all of its coefficients are entirely imaginary. Also, $T$ preserves $\bigH$ only if all of its coefficients are real. Thus, the isometry group of $\bigH$ is defined
$$Isom^+(\bigH) = \{ T(z) = \frac{az + b}{cz + d} \mid ad - bc = 1 \text{ and } a, d, b, c \in \R \}.$$

\noindent There are three different types of isometries in $Isom^+(\bigH)$: \\
\begin{enumerate}
	\item \textbf{Elliptic} transformations have a trace $< 2$. An elliptic transformation $E$ is of the form
	$$E = S \circ T_\theta \circ S^{-1} \ \ \text{  where  }\ \  T_\theta = \frac{\cos \theta z - \sin \theta}{\sin \theta z + \cos \theta}.$$\\
	These transformations have one fixed point in $\bigH$, which is the center of rotation.\\
	\begin{center}
	\includegraphics[scale=0.05]{elliptic}
	\end{center}
	\item \textbf{Parabolic} transformations have a trace $= 2$. A parabolic transformation $P$ is of the form 
	$$P = S \circ T \circ S^{-1} \ \ \text{ where } \ \ T = z + k \ \text{ with } k \in \R.$$\\
	Parabolic transformations have one fixed point in $\Rbar$.\\	
	\begin{center}
	\includegraphics[scale=0.055]{parabolic}
	\end{center}
	\item \textbf{Loxodromic} transformations have a trace $> 2$. A loxodromic transformation $L$ is of the form 
	$$L = S \circ T \circ S^{-1} \ \ \text{ where } \ \ T = \frac{kz + 0}{0 + 1/k} \approx kz  \ \text{ for } k \in \R.$$\\
	Loxodromic transformations have two fixed points in $\Rbar$.\\	
	\begin{center}
	\includegraphics[scale=0.06]{loxodromic}
	\end{center}
\end{enumerate}

You can classify transformations by finding their fixed points or their trace.\\

Make sure that you are able to do a calculation where you can make any of the three types of transformations by conjugating $T$ or $T_\theta$ (the basic transformation of each type) by some other transformation $S$.\\

\bigskip
\section{Measuring Distance in $\bigH$}
\bigskip

We defined that for some path $z: [a, b] \rightarrow \bigH$ defined $z(t) = x(t) + iy(t)$, we have
$$l_\bigH(z) = \int_a^b \frac{|z'(t)|}{y(t)} dt.$$

For hyperbolic distance, we use the basis that for two points $a, b$ on the imaginary axis with $a < b$, we have 
$$d_\bigH(a, b) = \ln\bigg(\frac{b}{a}\bigg).$$
Using this fact, we can find the distance between any two points in $\bigH$. Consider two points $P$ and $Q$ that lie on a hyperbolic line with endpoints $a$ and $b$. If we construct the transformation 
$$T(z) = \frac{z - a}{z - b}$$
that sends $a$ to 0 and $b$ to $\infty$, we know that $T$ sends $P$ and $Q$ to the imaginary axis as well. Thus, if $T(Q) > T(P)$, we see that 
$$d_\bigH(P, Q) = d_\bigH(T(P), T(Q)) = \ln\bigg(\frac{T(Q)}{T(P)}\bigg).$$\\

\bigskip
\section{Measuring Area in $\bigH$}
\bigskip

\noindent \textit{The Gauss-Bonnet Theorem:} \\
\indent If you have a hyperbolic triangle $T$ with interior angles $\alpha, \beta, \gamma$, then 
$$\mu_\bigH(T) = \pi - (\alpha + \beta + \gamma).$$\\

If a vertex of a triangle lies in $\Rbar$, then the interior angle at that vertex is 0.\\

We define the hyperbolic area of a region $R$ in $\bigH$ by 
$$\mu_\bigH(R) = \iint \limits_R \frac{1}{y^2} \ dxdy.$$\\

\begin{quote}\textit{
Ex) Consider the region $T$, a triangle with vertices $e^{i\theta}, e^{i\phi}$, and $\infty$. The two vertices in $\bigH$ lie on the unit circle. Thus, we have 
$$\mu_\bigH(T) = \int_{\cos \theta}^{\cos \phi} \lim_{t \to \infty} \int_{\sqrt{1 -x^2}}^t \ \frac{1}{y^2} \ dydx.$$
However, we also know from Homework 7 that the interior angle at $e^{i\theta}$ is $\theta$, and the interior angle at $e^{i\phi}$ is $\pi - \phi$. Thus, we can use Gauss-Bonnet to find that 
$$\mu_\bigH(T) = \pi - (\pi - \phi + \theta) = \phi + \theta.$$}
\end{quote}

\newpage

\noindent \textbf{\large The Poincar\'e Disk}

 \bigskip
\section{Introduction to the Poincar\'e Disk}
\bigskip

\begin{enumerate}
	\item The Poincar\'e Disk $\D$ is defined as $\D = \{ z : |z| < 1 \},$ or the interior of the unit circle in $\C$.\\
	\item The boundary at infinity of the Poincar\'e Disk is $|z| = 1$, the unit circle in $\C$.\\
	\item Thus, the distance from any point inside the disk to the boundary of the disk is infinite.\\
	\item The function $f : \bigH \rightarrow \D$ defined $f(z) = \frac{iz + 1}{z + i}$ maps the upper half plane to the Poincar\'e Disk and maps $\Rbar$ to $|z| = 1$.\\
	\item $f$ maps hyperbolic lines in $\bigH$ to hyperbolic lines in $\D$, namely diameters of the unit circle and semicircles orthogonal to the boundary at infinity.\\
	\item To find where $f$ sends the hyperbolic line with endpoints $a$ and $b$, we simply find the endpoints of the line in $\D$ by calculating $f(a)$ and $f(b)$.\\
\end{enumerate}

\bigskip
\section{Measuring in the Poincar\'e Disk}
\bigskip

The distance between two points $z, w \in \D$ is defined 
$$d_\D(z, w) = d_\bigH(f^{-1}(z), f^{-1}(w)).$$\\

The hyperbolic arc length can be found in two ways. Consider a path $\gamma(t) = x(t) + i(t)$ for $\gamma: [a, b] \rightarrow \D$ and two points $z, w \in \D$. We can say that either
$$l_\D(\gamma) = \int_a^b \frac{2}{1 - x^2(t) - y^2(t)} dt$$
or, alternatively,
$$l_\D(\gamma) = l_\bigH(f^{-1}(\gamma)).$$

Furthermore, if $\gamma$ is the hyperbolic line between $z$ and $w$, we can expand the latter to say 
$$l_\D(\gamma) = d_\D(z, w) = d_\bigH(f^{-1}(z), f^{-1}(w)).$$

If $\gamma$ is not the hyperbolic line between $z$ and $w$, then $l_\D(\gamma) > d_\D(z, w)$.\\

\bigskip
\section{Isometries of $\D$}
\bigskip

The isometry group of $\D$ is defined as 
$$Isom^+(\D) = f \circ Isom^+(\bigH) \circ f^{-1} = \{ f \circ T \circ f^{-1} \mid T \in Isom^+(\bigH) \}.$$
We see that these compositions will always map $\D$ to $\D$ since $f^{-1}: \D \rightarrow \bigH$, then $T \in Isom^+(\bigH): \bigH \rightarrow \bigH$, and finally $f: \bigH \rightarrow \D$.\\

Given some function $T \in Isom^+(\bigH)$ where $T(z) = \frac{az + b}{cz + d}$, we can find the composition $f \circ T \circ f^{-1}$ by noticing
\begin{align*}
f \circ T \circ f^{-1} &= \begin{pmatrix} i & 1 \\ 1 & i \end{pmatrix} \begin{pmatrix} a & b \\ c & d \end{pmatrix} \begin{pmatrix} i & 1 \\ 1 & i \end{pmatrix}^{-1}\\
&= \begin{pmatrix} i & 1 \\ 1 & i \end{pmatrix} \begin{pmatrix} a & b \\ c & d \end{pmatrix} \begin{pmatrix} i & -1 \\ -1 & i \end{pmatrix}\\
\end{align*}
and calculating using matrix multiplication.

\newpage

\noindent \textbf{\large Knowledge Questions}
\begin{enumerate}
	\item How do you normalize a transformation?\\
	\item What is the cross ratio of $z_0, z_1, z_2, z_3$?\\
	\item How can you construct a transformation which takes a triple of points $a, b, c$ to $1, 0,$ and $\infty$?\\
	\item How can you construct a transformation which takes a triple of points $a, b, c$ to another triple of points $x, y, z$?\\
	\item How do you construct the hyperbolic line between two points $P$ and $Q$ in $\bigH$?\\
	\item Which transformations preserve $\bigH$? Which preserve $\Rbar$? \\
	\item Which transformations have trace equal to 2? Which have trace less than 2? Greater than 2?\\
	\item Which transformations have two fixed points in $\Rbar$? One fixed point in $\Rbar$?\\
	\item Which transformations are rotations around a fixed point?\\
	\item Which transformations take the form of $STS^{-1}$ where $T(z) = kz$ for $k \in \R$?\\
	\item What about where $T(z) = z + k$ for $k \in \R$? Where $T(z) = \frac{\cos \theta z - \sin \theta}{\sin \theta + \cos \theta}$?\\
	\item What is a horocycle and which transformation can take the form of a horocycle?\\
	\item Which transformations have an axis of translation?\\
	\item For a path $z(t) = x(t) + iy(t)$ with $z : [a, b] \rightarrow \bigH$, what is $l_\bigH(z)$?\\
	\item For two points on the imaginary axis with $a < b$, what is $d_\bigH(a, b)$?\\
	\item What is the form of a  parameterization of a circle?
	\item For a path $\gamma : [a, b] \rightarrow \bigH$ such that $\gamma$ is not equal to the hyperbolic line between $a$ and $b$, what is the relationship between $l_\bigH(\gamma)$ and $d_\bigH(a, b)$?\\
	\item How would you construct a transformation that sends the hyperbolic line between $a$ and $b$ to the imaginary axis? \\
	\item How would you find the hyperbolic distance between two points $P$ and $Q$ not on the imaginary axis?\\
	\item What is the Gauss-Bonnet Theorem?\\
	\item In a hyperbolic triangle, if one of the vertices lies in $\Rbar$, what is the interior angle at that vertex?\\
	\item What is the equation for hyperbolic area of a region $R$ in $\bigH$?\\
	\item How is the Poincar\'e Disk defined, and what is its boundary at infinity?\\
	\item What is the distance from any point inside the disk to the boundary at infinity?\\
	\item How do we define $f$, the function that maps $\bigH$ to $\D$ and $\Rbar$ to the boundary at infinity?\\
	\item Where in general does $f$ send hyperbolic lines in $\bigH$?\\
	\item How can we find where $f$ sends a particular hyperbolic line in $\bigH$?\\
	\item For two points $z, w \in \D$, what is $d_\D(z, w)$? \\
	\item For a path $\gamma(t) = x(t) + iy(t)$ with $\gamma: [a, b] \rightarrow \D$, what are two ways to find $l_\D(\gamma)$?\\
	\item If the path $\gamma$ above happens to be the hyperbolic line between $z$ and $w$, how else can we describe $l_\D(\gamma)$?\\
	\item If $\gamma$ is not the hyperbolic line between $z$ and $w$, how does $l_\D(\gamma)$ compare to $d_\D(z, w)$?\\
	\item How do we define the isometry group of $\D$?\\
	\item Given some function $T$ which is an isometry of $\bigH$, how can we find the composition $f \circ T \circ f^{-1}$?\\
\end{enumerate}

\newpage
\noindent \textbf{\large Practice Questions}
\begin{enumerate}
	\item Normalize the transformations $T(z) = \frac{4z + 4}{5z - 4}$, $S(z) = z + k$ with $k \in \R$, and $L(z) = \frac{1}{z}$. Which of these transformations preserve $\bigH$? $\Rbar$? If any of them are isometries, classify the isometries.\\
	\item What is the cross ratio of $i, 1, 4,$ and $2i$?\\
	\item Find a Mobius transformation sending 1, 0, and $\infty$ to $4, i,$ and -1. \\
	\item Find a Mobius transformation sending 0, $i$, and $-i$ to 0, 1, and 2.\\
	\item Describe the hyperbolic line passing through $i$ and $1 + 2i$, and note its endpoints at infinity. \\
	\item Classify the following isometries by finding the fixed points. If elliptic, give the center of rotation; if loxodromic, give the axis of translation. $T(z) = 7z + 6$, $S(z) = \frac{z}{z + 1}$, $M(z) = \frac{-1}{z}$. \\
	\item As above, classify the following isometries. This time, use the trace. $T(z) = \frac{7z - 6}{3z - 2}$, $S(z) = \frac{-5z + 12}{-3z + 7}$, $M(z) = \frac{-z-1}{2z + 1}$.\\
	\item Find the hyperbolic arc length of path $g: [0, 1] \rightarrow \bigH$ defined $g(t) = i(t^2 + 1)$. \\
	\item Find $d_\bigH(-3 + i, 2 + i)$. \\
	\item Calculate the hyperbolic area of the region $R$ bounded by $Re(z) = -1$, $Re(z) = 1$, and $Im(z) = 1$ that lies above $Im(z) =1$.\\
	\item Calculate the hyperbolic area of the triangle $T$ with vertices $e^{i\theta}$, $e^{i\phi}$, and $\infty$.\\
	\item Describe where $f: \bigH \rightarrow \D$ sends the imaginary axis in $\bigH$. \\
	\item Describe where $f$ sends the hyperbolic line from 1 to 2 in $\bigH$.\\
	\item Calculate $d_\D(0, 1/2)$. \\
	\item For $\gamma(t) = t$ with $\gamma:[0, r] \rightarrow \D$ and $r < 1$, calculate $l_\D(\gamma)$.\\
	\item For $\gamma(t) = it$ with $\gamma:[-1/2, 1/2] \rightarrow \D$, calculate $l_\D(\gamma)$ using the integral definition. You will need to use partial fractions. Then, calculate $l_\D(\gamma)$ again by translating to $\bigH$. \\
	\item Compute $f \circ T \circ f^{-1}$ for $T(z) = \frac{2z + 3}{4z - 1}$.\\
\end{enumerate}

\bigskip
\noindent \textbf{\large Practice Question Answers}
\begin{enumerate}
	\item $T(z) = \frac{2/3iz + 2/3i}{5/6iz - 2/3i}$, $S(z) = z + k$, and $L(z) = \frac{-i}{-iz}$. $T$ preserves $\Rbar$ but not $\bigH$; $S$ preserves $\bigH$; and $L$ preserves $\Rbar$ but not $\bigH$. Since $S$ has one fixed point at infinity, it is a parabolic transformation. \\
	\item $\frac{(i - 4)(1 - 2i)}{(i - 2i)(1 - 4)}.$\\
	\item $M(z) = \frac{(4 - i)z + 5}{(i - 4)z + 5}$.\\
	\item $M(z) = \frac{4z}{3z +i}$.\\
	\item $|z - 2| = \sqrt 5$; endpoints at infinity are $2 + \sqrt 5, 2 - \sqrt 5$.\\
	\item $T$ is loxodromic with axis of translation $Re(z) = 1$; $S$ is parabolic; $M$ is elliptic with center of rotation $i$.\\
	\item $T$ is loxodromic with axis of translation $|z - 3/2| = 1/2$; $S$ is parabolic; and $M$ is elliptic with center of rotation $-1/2 + 1/2i$.\\
	\item $l_\bigH(g) = \ln 2$.\\
	\item $d_\bigH(-3 + i, 2 + i) = ?$\\
	\item $\mu_\bigH(R) = 2$.\\
	\item $\mu_\bigH(T) = \phi + \theta$.\\
	\item It sends the imaginary axis to the diameter from -1 to 1.\\
	\item It sends that line to the line between 1 and $4/5 + 3/5i$.\\
	\item $d_\D(0, 1/2) = d_\bigH(i, 3/5 + 4/5i) = ?$ \\
	\item $l_\D(\gamma) = \ln \abs{\frac{1 + r}{1 - r}}$\\
	\item $l_\D(\gamma) = \ln 9$.\\
	\item $f \circ T \circ f^{-1} = ?$.\\
\end{enumerate}




\end{document}